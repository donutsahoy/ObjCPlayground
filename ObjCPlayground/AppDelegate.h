//
//  AppDelegate.h
//  ObjCPlayground
//
//  Created by Robert J. Sarvis Jr on 2/22/23.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

